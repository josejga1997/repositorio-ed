﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poligonos
{
    public class Cuadrado
    {
        //Propiedad lado 
        public double Lado { get; set; }
        //Propiedad area solo lectura
        public double Area { get; }
    }
}
